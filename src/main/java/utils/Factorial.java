package utils;

import java.math.BigInteger;

public abstract class Factorial {

    /*Метод возвращает факториал n вычисленный рекурсивно
    * @param n число
    * @return int
    * */
    public static BigInteger countFactorialRec(int n){
        BigInteger factorial = BigInteger.valueOf(0);
       if((n == 1)||(n == 0)){
           return BigInteger.valueOf(1);
       } else {
           factorial = countFactorialRec(n - 1).multiply(BigInteger.valueOf(n));
       }
       return factorial;
    }

    /*Метод возвращает факториал n
     * @param n число
     * @return int
     * */
    public static BigInteger countFactorial(int n){
        BigInteger factorial = BigInteger.valueOf(1);
        if((n == 1)||(n == 0)){
            return factorial;
        } else {
            for(int i = 2; i <= n; i++){
                factorial = factorial.multiply(BigInteger.valueOf(i));
            }
        }
        return factorial;
    }
}
