package utils;

import java.io.*;
import java.util.ArrayList;


public abstract class FileUtil {


    /*Метод создает txt файл и записывает в него числа из массива
     * @param fileName имя файла
     * @param path директория для сохранения файла
     * @param ints массив с числами
     **/
    public static void writeTxt(String filePath, ArrayList<Integer> ints) {
        try {
            File file = new File(filePath);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));

            if (ints.size() > 0){
                writer.write(ints.get(0).toString());
            }
            for(int i = 1; i < ints.size(); i++){
                writer.append(",").write(ints.get(i).toString());
            }

            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Метод считывает из txt файла строку
     * @param filePath путь к файлу
     * @return String
     **/
    public static String readTxt(String filePath){
        String line = "";
        try {
            File file = new File(filePath);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            line = reader.readLine();
            reader.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }




}
