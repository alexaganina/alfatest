package utils;

import java.util.ArrayList;

public abstract class ArrayUtil {

    /*Метод создает массив с числами от 0 до n
     * @param n количество
     * @return ArrayList
     **/
    public static ArrayList<Integer> createIntArrayList(int n){
        ArrayList <Integer> ints = new ArrayList<Integer>();
        for (int i = 0; i <= n; i++){
            ints.add(i);
        }
        return ints;
    }

    /*Метод преобразует строку формата *,*,*... в ArrayList
     * @param str строка с числами
     * @return ArrayList
     **/
    public static ArrayList<Integer> convertStringToIntArrayList(String str){
       String[] strArr = str.split(",");
        ArrayList<Integer> ints = new ArrayList <Integer>();
       for (int i = 0; i < strArr.length; i++){
           ints.add(Integer.valueOf(strArr[i]));
       }
        return ints;
    }

}
