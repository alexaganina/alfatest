package exercise;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static utils.ArrayUtil.*;
import static utils.FileUtil.*;



public class exercise1 {

    @Test
    public void sortInts(){
        int n = 20;
        ArrayList<Integer> ints = createIntArrayList(n);
        Collections.shuffle(ints);

        String fileName = "Ints_" + (new Date()).toString().replaceAll("[ :]", "_");
        String path = System.getProperty("user.dir") + "\\src\\main\\resources\\testfiles";
        String filePath = path + "\\" + fileName + ".txt";
        writeTxt(filePath, ints);

        System.out.println("Значения по возрастанию: ");
        ArrayList<Integer> intsFromFile = convertStringToIntArrayList(readTxt(filePath));
        Collections.sort(intsFromFile);
        System.out.println(intsFromFile);

        System.out.println("Значения по убыванию: ");
        intsFromFile = convertStringToIntArrayList(readTxt(filePath));
        Collections.sort(intsFromFile, Collections.<Integer>reverseOrder());
        System.out.println(intsFromFile);
    }
}
